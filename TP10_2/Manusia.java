package TP10_2;

public abstract class Manusia implements Makhluk{
    public String nama;
    public int uang;

    protected Manusia(String nama, int uang){
        this.nama=nama;
        this.uang = uang;
    }

    protected Manusia(){
        
    }
    public String getNama(){
        return nama;
    }
    public int getUang(){
        return uang;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public void setUang(int uang){
        this.uang=uang;
    }
    public abstract String bicara();

    @Override
    public String bernafas(){
        String bernafas = this.nama+" bernafas dengan paru-paru";
        return bernafas;
    }

    @Override
    public String bergerak(){
        String bergerak = this.nama+" bergerak dengan cara berjalan";
        return bergerak;
    }
}
