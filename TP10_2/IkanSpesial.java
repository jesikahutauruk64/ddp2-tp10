package TP10_2;

public class IkanSpesial extends Ikan implements bisaTerbang{
    public IkanSpesial(){

    }
    public IkanSpesial(String nama, String spesies, boolean isBeracun){
        setIsBeracun(isBeracun);
        setNama(nama);
        setSpesies(spesies);
    }

    public String bersuara(){
        String bersuara ="";
        if(isBeracun == true){
            bersuara = "Blub blub blub blub. Blub. Blub blub blub.(Halo, saya "
            + this.nama +". Saya ikan yang beracun. Saya bisa terbang loh.).";
        }
        else{
            bersuara = "Blub blub blub blub. Blub. Blub blub blub.(Halo, saya "
            + this.nama +". Saya ikan yang tidak beracun. Saya bisa terbang loh.).";
        }
        return bersuara;
    }

    @Override
    public String terbang(){
        return "Fwooosssshhhhh! Plup.";
    }
        
}
