package TP10_2;

public abstract class Hewan implements Makhluk{
    public String nama;
    public String spesies;

    protected Hewan(){
        
    }
    protected Hewan(String nama, String spesies){
        this.nama = nama;
        this.spesies = spesies;
    }
    public String getNama(){
        return nama;
    }
    public String getSpesies(){
        return spesies;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public void setSpesies(String spesies){
        this.spesies=spesies;
    }
    public abstract String bersuara();
}
