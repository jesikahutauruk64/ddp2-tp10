package TP10_2;
import java.util.*;

public class Main {
    public static void main(String[] args){
        ArrayList<Manusia> manusia = new ArrayList<Manusia>();
        ArrayList<Hewan> hewan = new ArrayList<Hewan>();

        Manusia bujang = new Pegawai("Bujang", 100000, "pemula");
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master"); 
        Hewan kerapu = new Ikan("Kerapu Batik", "Epinephelus Polyphekadion", false);
        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);

        manusia.add(yoga);
        manusia.add(bujang);
        hewan.add(kerapu);
        hewan.add(ikanTerbang);

        ArrayList<String> makhlukManusia = new ArrayList<String>(
            Arrays.asList("yoga","bujang")
        );
        ArrayList<String> makhlukManusiaNama = new ArrayList<String>(
            Arrays.asList("Yoga","Bujang")
        );
        ArrayList<String> makhlukHewan = new ArrayList<String>(
            Arrays.asList("kerapu","ikanTerbang")
        );
        ArrayList<String> makhlukHewanNama = new ArrayList<String>(
            Arrays.asList("Kerapu Batik","Ikan Terbang Biru")
        );

        String perintah="";
        Scanner input = new Scanner(System.in);

        while(!perintah.equalsIgnoreCase("Selesai")){
            System.out.print("Masukkan perintah: ");
            perintah = input.nextLine();

            if(perintah.equalsIgnoreCase("selesai")){
                break;
            }
            else{
                try{
                    int idx = perintah.lastIndexOf(" ");
                    String nama = perintah.substring(0,idx);
                    String kegiatan = perintah.substring(idx+1,perintah.length());
                    if(makhlukManusia.contains(nama) || makhlukManusiaNama.contains(nama)){
                        int index=0;
                        if(!makhlukManusia.contains(nama)){
                            index = makhlukManusiaNama.indexOf(nama);
                        }else{
                            index = makhlukManusia.indexOf(nama);
                        }
                        try{
                            if(kegiatan.equalsIgnoreCase("bernafas")){
                                System.out.println(manusia.get(index).bernafas());
                            }
                            else if(kegiatan.equalsIgnoreCase("bergerak")){
                                System.out.println(manusia.get(index).bergerak());
                            }
                            else if(kegiatan.equalsIgnoreCase("bicara")){
                                System.out.println(manusia.get(index).bicara());
                            }
                            else if(kegiatan.equalsIgnoreCase("bekerja")){
                                System.out.println(((Pegawai)manusia.get(index)).bekerja());
                            }
                            else if(kegiatan.equalsIgnoreCase("beli")){ 
                                System.out.println(((Pelanggan)manusia.get(index)).membeli());
                            }
                            else if(kegiatan.equalsIgnoreCase("libur")){ 
                                System.out.println(((PegawaiSpesial)manusia.get(index)).libur());
                            }
                            else{
                                if(manusia.get(index).getClass().getSimpleName().equals("PegawaiSpesial")){
                                    System.out.println("Maaf, Pegawai Spesial "+
                                manusia.get(index).getNama()+" tidak bisa "+kegiatan);
                                }
                                else{
                                    System.out.println("Maaf,"+manusia.get(index).getClass().getSimpleName()+" "+
                                manusia.get(index).getNama()+" tidak bisa "+kegiatan);
                                }
                            }
                        }
                        catch(ClassCastException ex){
                            if(manusia.get(index).getClass().getSimpleName().equals("PegawaiSpesial")){
                                System.out.println("Maaf, Pegawai Spesial "+
                            manusia.get(index).getNama()+" tidak bisa "+kegiatan);
                            }
                            else{
                                System.out.println("Maaf,"+manusia.get(index).getClass().getSimpleName()+" "+
                            manusia.get(index).getNama()+" tidak bisa "+kegiatan);
                            }
                        }
    
                    }else if(makhlukHewan.contains(nama)||makhlukHewanNama.contains(nama)){
                        int index=0;
                        if(!makhlukHewan.contains(nama)){
                            index = makhlukHewanNama.indexOf(nama);
                        }else{
                            index = makhlukHewan.indexOf(nama);
                        }
                    
                        if(kegiatan.equalsIgnoreCase("bernafas")){
                            System.out.println(hewan.get(index).bernafas());
                        }
                        else if(kegiatan.equalsIgnoreCase("bergerak")){
                            System.out.println(hewan.get(index).bergerak());
                        }
                        else if(kegiatan.equalsIgnoreCase("bersuara")){
                            System.out.println(hewan.get(index).bersuara());
                        }
                        else if(kegiatan.equalsIgnoreCase("terbang")){ 
                            try{
                                System.out.println(((IkanSpesial)hewan.get(index)).terbang());
                            }
                            catch(ClassCastException ex){
                                if(hewan.get(index).getClass().getSimpleName().equals("IkanSpesial")){
                                    System.out.println("Maaf, Ikan Spesial "+
                                hewan.get(index).getSpesies()+" tidak bisa "+kegiatan);
                                }
                                else{
                                    System.out.println("Maaf,"+hewan.get(index).getClass().getSimpleName()+" "+
                                hewan.get(index).getSpesies()+" tidak bisa "+kegiatan);
                                }
                            }
                        }
                        else{
                            if(hewan.get(index).getClass().getSimpleName().equals("IkanSpesial")){
                                System.out.println("Maaf, Ikan Spesial "+
                            hewan.get(index).getSpesies()+" tidak bisa "+kegiatan);
                            }
                            else{
                                System.out.println("Maaf,"+hewan.get(index).getClass().getSimpleName()+" "+
                            hewan.get(index).getSpesies()+" tidak bisa "+kegiatan);
                            }
                        }
                    }else{
                        System.out.println("Tidak ada makhluk bernama "+nama);
                    }
                }
                catch(StringIndexOutOfBoundsException e){
                    System.out.println("Maaf, perintah tidak ditemukan!");
                }
            }
        }
        input.close();
        System.out.println("Sampai jumpa!");
    }
}