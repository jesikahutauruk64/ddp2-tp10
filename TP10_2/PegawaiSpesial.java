package TP10_2;

public class PegawaiSpesial extends Pegawai implements bisaLibur{
    public PegawaiSpesial(){}

    public PegawaiSpesial(String nama, int uang, String levelKeahian){
        setLevelKeahlian(levelKeahian);
        setNama(nama);
        setUang(uang);
    }

    public String bicara(){
        String pegawaiBicara = "Halo, saya "+ this.nama + ". Uang saya adalah "+ this.uang
        +", dan level keahlian saya adalah "+ this.levelKeahlian+". Saya memiliki privilage bisa libur.";
        return pegawaiBicara;

    }
    
    @Override
    public String libur(){
        String libur = this.nama+" sedang berlibur ke Akihabara";
        return libur;
    }
    
}
