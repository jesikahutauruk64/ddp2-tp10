package TP10_2;

public class Ikan extends Hewan{
    public boolean isBeracun;

    public Ikan(){
        
    }
    public Ikan(String nama, String spesies, boolean isBeracun){
        this.isBeracun = isBeracun;
        setNama(nama);
        setSpesies(spesies);
    }
    
    public boolean getIsBeracun(){
        return isBeracun;
    }
    public void setIsBeracun(boolean isBeracun){
        this.isBeracun = isBeracun;
    }
    public String bersuara(){
        String bersuara ="";
        if(isBeracun == true){
            bersuara = "Blub blub blub blub. Blub. (Halo, saya "+this.nama+". Saya ikan yang beracun).";
        }
        else{
            bersuara = "Blub blub blub blub. Blub. (Halo, saya "+this.nama+". Saya ikan yang tidak beracun).";
        }
        return bersuara;
        
    }

    @Override
    public String bernafas(){
        String bernafas = this.nama+" bernafas dengan insang";
        return bernafas;
    }

    @Override
    public String bergerak(){
        String bergerak = this.nama+" bergerak dengan cara berenang";
        return bergerak;
    }
}
