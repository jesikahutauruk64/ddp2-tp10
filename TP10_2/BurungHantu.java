package TP10_2;

public class BurungHantu extends Hewan{
    public BurungHantu(){
        
    }
    public BurungHantu(String nama, String spesies){
        setNama(nama);
        setSpesies(spesies);
    }
    public String bersuara(){
        String bersuara="Hooooh hoooooooh.(Halo, saya "+this.nama+". Saya adalah burung hantu).";
        return bersuara;
    }

    @Override
    public String bernafas(){
        String bernafas = this.nama+" bernafas dengan paru-paru";
        return bernafas;
    }

    @Override
    public String bergerak(){
        String bergerak = this.nama+" bergerak dengan cara terbang";
        return bergerak;
    }
}
