package TP10_2;

public class Pegawai extends Manusia{
    public String levelKeahlian;

    public Pegawai(){
        
    }
    
    public Pegawai(String nama, int uang, String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
        setNama(nama);
        setUang(uang);
    }
    public void setLevelKeahlian(String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
    }

    public String getLevelKeahlian(){
        return levelKeahlian;
    }
 
    public String bekerja(){
        String bekerja = this.nama+" bekerja di kedai VoidMain.";
        return bekerja;
    }

    public String bicara(){
        String pegawaiBicara = "Halo, saya "+ this.nama + ". Uang saya adalah "+ this.uang+", dan level keahlian saya adalah "+ this.levelKeahlian+".";
        return pegawaiBicara;

    }
    
}
