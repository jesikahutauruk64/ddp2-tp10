package TP10_2;

public class Pelanggan extends Manusia{
    public Pelanggan(){
        
    }
    public Pelanggan(String nama, int uang){
        setNama(nama);
        setUang(uang);
    }
    public String bicara(){
        String pelangganBicara = "Halo, saya "+ this.nama + ". Uang saya adalah "+ this.uang+".";
        return pelangganBicara;
    }

    public String membeli(){
        String membeli =this.nama+" membeli makanan dan minuman di kedai VoidMain.";
        return membeli;
    }
}
